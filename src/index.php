<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon Site Web</title>
</head>
<body>

    <h1>Bienvenue sur mon site web</h1>

    <?php
    // Voici un exemple de code PHP pour afficher la date et l'heure actuelles
    echo "<p>Il est actuellement " . date("H:i:s") . ".</p>";
    ?>

</body>
</html>
